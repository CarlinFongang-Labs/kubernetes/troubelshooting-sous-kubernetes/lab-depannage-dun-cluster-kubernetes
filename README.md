# Lab : Dépannage d'un cluster Kubernetes

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Déterminer ce qui ne va pas avec le cluster

2. Résoudre le problème

# Contexte 

Votre entreprise, BeeBox, dispose d'un nouveau cluster Kubernetes qui vient d'être construit par un entrepreneur externe. Hier soir, quelqu'un a redémarré les serveurs utilisés pour exécuter ce cluster. Depuis le redémarrage, certains membres de votre équipe signalent des problèmes avec l'un des nœuds de travail. Malheureusement, l’entrepreneur ne travaille plus avec l’entreprise, vous devrez donc rechercher et résoudre le problème.

Explorez le cluster et déterminez la cause des problèmes. Prenez ensuite des mesures pour résoudre le problème et assurez-vous qu'il ne se reproduise plus.

>![Alt text](img/image.png)

# Introduction
Chaque administrateur Kubernetes devra probablement gérer un cluster en panne à un moment donné, qu'il s'agisse d'un seul nœud ou de l'ensemble du cluster. Dans cet atelier, vous pourrez mettre en pratique vos compétences en dépannage. Un cluster Kubernetes défectueux vous sera présenté et il vous sera demandé d'utiliser vos compétences d'investigation pour identifier le problème et le résoudre.


# Application

## Étape 1 : Connexion au Serveur du Nœud du Plan de Contrôle

Connectez-vous au serveur de nœud du plan de contrôle à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Déterminer ce qui ne va pas avec le Cluster

1. Découvrez quel nœud rencontre un problème :

```sh
kubectl get nodes
```

>![Alt text](img/image-1.png)

   - Identifiez si un nœud est dans l’état `NotReady`.

2. Obtenez plus d'informations sur le nœud :

```sh
kubectl describe node k8s-worker2
```

3. Recherchez la section `Conditions` des informations sur le nœud et découvrez ce qui affecte l'état du nœud et provoque son échec.

>![Alt text](img/image-2.png)

L'on constate dans les conditions qui nous sont affichées le message : **"NodeStatusUnknown |  Kubelet stopped posting node status."**

Conditions:
  Type           |  Status  |  LastHeartbeatTime                | LastTransitionTime              |  Reason            |  Message
  ----           |  ------  |  -----------------                | ------------------              |  ------            |  -------
  MemoryPressure |  Unknown |  Sat, 01 Jun 2024 08:57:43 +0000  | Sat, 01 Jun 2024 08:59:03 +0000 |  NodeStatusUnknown |  Kubelet stopped posting node status.
  DiskPressure   |  Unknown |  Sat, 01 Jun 2024 08:57:43 +0000  | Sat, 01 Jun 2024 08:59:03 +0000 |  NodeStatusUnknown |  Kubelet stopped posting node status.
  PIDPressure    |  Unknown |  Sat, 01 Jun 2024 08:57:43 +0000  | Sat, 01 Jun 2024 08:59:03 +0000 |  NodeStatusUnknown |  Kubelet stopped posting node status.
  Ready          |  Unknown |  Sat, 01 Jun 2024 08:57:43 +0000  | Sat, 01 Jun 2024 08:59:03 +0000 |  NodeStatusUnknown |  Kubelet stopped posting node status.

Nous allons ainsi investiguer sur kubelet une fois connecté au worker2

## Étape 3 : Consultation des Journaux du Kubelet

1. Connectez-vous au serveur du nœud Worker 2 à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@PUBLIC_IP_ADDRESS_WORKER_NODE2
```

2. Consultez les journaux kubelet du nœud Worker 2 :

```sh
sudo journalctl -u kubelet
```

3. Allez à la fin du journal en appuyant sur `Shift + G` et voyez les messages d'erreur indiquant que kubelet s'est arrêté.

>![Alt text](img/image-3.png)

4. Examinez l'état du kubelet :

```sh
sudo systemctl status kubelet
```

- Notez si le service kubelet est en cours d'exécution ou non.

>![Alt text](img/image-4.png)
*Service en arret*

## Étape 4 : Résoudre le Problème

1. Afin de résoudre le problème, nous devons non seulement démarrer le serveur, mais également activer kubelet pour garantir qu'il continue de fonctionner si le serveur redémarre à l'avenir :

```sh
sudo systemctl enable kubelet
sudo systemctl start kubelet
```

>![Alt text](img/image-5.png)


2. Vérifiez si kubelet est actif :

```sh
sudo systemctl status kubelet
```

   - Notez si le service est répertorié comme actif (en cours d'exécution).

>![Alt text](img/image-6.png)
*Service actif*

## Étape 5 : Vérifier l'État des Nœuds

1. Revenez au nœud du plan de contrôle.

2. Vérifiez si tous les nœuds sont maintenant dans l’état `Ready` :

```sh
kubectl get nodes
```

   - Vous devrez peut-être attendre et essayer la commande plusieurs fois avant que tous les nœuds apparaissent comme `Ready`.

>![Alt text](img/image-7.png)
*k8s-woker2 pret*

Ce laboratoire vous guide à travers le processus de dépannage d'un cluster Kubernetes en panne en identifiant les problèmes de nœud, en consultant les journaux kubelet et en redémarrant et activant le service kubelet. Vous serez ainsi en mesure de gérer efficacement les pannes de nœuds dans un cluster Kubernetes.